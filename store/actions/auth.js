export const SIGNUP = "SIGNUP";
export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";
import firebase from "../../config/firebase";

export const signup = (email, password) => {
  return async (dispatch) => {
    const response = await fetch(
      "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyCwPrhj0lMJP5nDhru8xSeoTEeacioPCDU",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: email,
          password: password,
          returnSecureToken: true,
        }),
      }
    );

    if (!response.ok) {
      throw new Error("Hata Oluştu!!!");
    }

    const resData = await response.json();
    dispatch({ type: SIGNUP, token: resData.idToken, userId: resData.localId });
  };
};

export const login = (email, password) => {
  if(!password){
    password="123456";
  }
  return async (dispatch) => {
    try {
      const resData = await firebase
        .auth()
        .signInWithEmailAndPassword(email, password);
      dispatch({
        type: LOGIN,
        userId: resData.user.uid,
      });
    } catch (error) {
      throw new Error("Hatalı giriş.Tekrar deneyiniz.");
    }
  };
};

export const logout = () => {
  return async (dispatch) => {
    firebase
      .auth()
      .signOut()
      .then(function () {
        dispatch({
          type: LOGOUT,
        });
      })
      .catch(function (error) {
        throw new Error("Hatalı oluştur.Tekrar deneyiniz.");
      });
  };
};
