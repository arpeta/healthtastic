import React from "react";
import LoginScreen from "../screens/LoginScreen";
import DashboardScreen from "../screens/DashboardScreen";
import SignUpScreen from "../screens/SignUpScreen";
import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { Ionicons } from "@expo/vector-icons";
import Colors from "../constants/Colors";
import { createDrawerNavigator, DrawerItems } from "react-navigation-drawer";

const defaultStackNavOptions = {
  headerStyle: {
    backgroundColor: Platform.OS === "android" ? Colors.accentColor : "",
  },
  // headerTitleStyle: {
  //   fontFamily: "roboto-bold",
  // },
  // headerBackTitleStyle: {
  //   fontFamily: "roboto-light",
  // },
  headerTintColor: Platform.OS === "android" ? "white" : "#009387",
  headerTitle: "Sayfa",
};

const HealthNavigator = createStackNavigator(
  {
    Dashboard: DashboardScreen,
    SignUp: SignUpScreen,
  },
  {
    defaultNavigationOptions: defaultStackNavOptions,
  }
);

const MainNavigator = createDrawerNavigator(
  {
    HealthTab: {
      screen: HealthNavigator,
      navigationOptions: {
        drawerLabel: "Anasayfa",
        drawerIcon: (drawerConfig) => (
          <Ionicons name="ios-home" size={21} color={drawerConfig.tintColor} />
        ),
      },
    },
  },
  {
    contentOptions: {
      activeTintColor: "#009387",
      labelStyle: {
        // fontFamily: "roboto-bold",
      },
    },
  }
);

const SwitchNav = createSwitchNavigator({
  Login: LoginScreen,
  Tabs: MainNavigator,
});

export default createAppContainer(SwitchNav);
