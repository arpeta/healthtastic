import React, { useState } from "react";
import * as Font from "expo-font";
import ReduxThunk from "redux-thunk";
import { AppLoading } from "expo-app-loading";
import FlashMessage from "react-native-flash-message";
import MainNavigator from "./navigation/MainNavigator";
import { Provider } from "react-redux";
import { combineReducers, createStore, applyMiddleware } from "redux";
import authReducer from "./store/reducers/auth";

const rootReducer = combineReducers({
  auth: authReducer,
});

const store = createStore(rootReducer, applyMiddleware(ReduxThunk));


export default function App() {

  return (
    <Provider store={store}>
      <MainNavigator />
      <FlashMessage position="top" />
    </Provider>
  );
}
