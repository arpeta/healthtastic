export default {
    primaryColor: "#4a148c",
    accentColor: "#ff6f00",
    BLACK: "#000",
    WHITE: "#FFF",
    DODGER_BLUE: "#428AF8",
    SILVER: "#BEBEBE",
    TORCH_RED: "#F8262F",
    MISCHKA: "#E5E4E6"
  };
  