import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Platform,
  TextInput,
  KeyboardAvoidingView,
  ActivityIndicator,
  Alert,
} from "react-native";
import * as Animatable from "react-native-animatable";
import { LinearGradient } from "expo-linear-gradient";
import Feather from "react-native-vector-icons/Feather";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import Colors from "../constants/Colors";
import { useTheme } from "react-native-paper";
import Animated from "react-native-reanimated";

const LoginScreen = (props) => {
  const [isLoading, setIsLoading] = useState(false);

  const authHandler = async () => {
    setIsLoading(true);
    try {
      props.navigation.navigate("Tabs");
    } catch (error) {
      Alert.alert("Hata", "Hata oluştu çıkıp tekrar deneyin!!!", [
        { text: "Tamam" },
      ]);
      setIsLoading(false);
      return;
    }
    setIsLoading(false);
  };

  const { colors } = useTheme();
  return (
    <KeyboardAvoidingView>
      <LinearGradient colors={["#FFFFFF", "#00bfe0"]} style={styles.gradient}>
        <Animatable.View animation="fadeInDownBig" style={styles.container}>
          <View style={styles.form}>
            <Text style={styles.text_footer}>Email</Text>
            <View style={styles.action}>
              <FontAwesome name="user-o" color="#05375a" size={20} />
              <TextInput
                placeholder="Email Adresiniz"
                style={styles.textInput}
                autoCapitalize="none"
              />
              <Feather name="check-circle" color="green" size={2} />
            </View>
            <Text style={[styles.text_footer, { marginTop: 35 }]}>Şifre</Text>
            <View style={styles.action}>
              <FontAwesome name="lock" color="#05375a" size={20} />
              <TextInput
                placeholder="Şifreniz"
                style={styles.textInput}
                autoCapitalize="none"
                secureTextEntry={true}
              />
              <Feather name="eye-off" color="grey" size={2} />
            </View>
            <View style={styles.button}>
              {isLoading ? (
                <ActivityIndicator
                  style={{ marginTop: 10 }}
                  size="large"
                  color="#FF5A00"
                />
              ) : (
                <TouchableOpacity style={styles.signIn} onPress={authHandler}>
                  <LinearGradient
                    colors={["#08d4c4", "#00bfe0"]}
                    style={[
                      styles.signIn,
                      {
                        marginTop: 50,
                      },
                    ]}
                  >
                    <Text style={[styles.textSign, { color: "#fff" }]}>
                      Giriş Yap
                    </Text>
                  </LinearGradient>
                </TouchableOpacity>
              )}
            </View>
            <TouchableOpacity
              onPress={() => props.navigation.navigate("Tabs")}
              style={[
                styles.signIn,
                {
                  borderColor: "#009387",
                  borderWidth: 1,
                  marginTop: 40,
                },
              ]}
            >
              <Text
                style={[
                  styles.textSign,
                  {
                    color: "#009387",
                  },
                ]}
              >
                Kaydol
              </Text>
            </TouchableOpacity>
          </View>
        </Animatable.View>
      </LinearGradient>
    </KeyboardAvoidingView>
  );
};

LoginScreen.navigationOptions = (navData) => {
  return {
    headerTitle: "Giriş Yap",
    headerStyle: {
      backgroundColor: "#ff9c71",
    },
    headerTintColor: "#fff",
  };
};

export const styles = StyleSheet.create({
  container: {
    marginRight: 30,
    marginLeft: 30,
    marginTop: 60,
    marginBottom: 60,
    flex: 1,
    backgroundColor: Colors.WHITE,
    borderRadius: 30,
    elevation: 5,
    shadowColor: "black",
    shadowOffset: { width: 30, height: 30 },
    shadowRadius: 20,
    shadowOpacity: 0.3,
    padding: 20,
  },
  gradient: {
    width: "100%",
    height: "100%",
    justifyContent: "center",
  },
  form: {
    flex: 2,
    justifyContent: "center",
    width: "100%",
  },
  header: {
    flex: 1,
    justifyContent: "flex-end",
    paddingHorizontal: 20,
    paddingBottom: 50,
  },
  footer: {
    backgroundColor: "#fff",
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  text_header: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 30,
    marginTop: 20,
  },
  text_footer: {
    color: "#05375a",
    fontSize: 18,
  },
  action: {
    flexDirection: "row",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#f2f2f2",
    paddingBottom: 5,
  },
  actionError: {
    flexDirection: "row",
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: "#FF0000",
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === "ios" ? 0 : -12,
    paddingLeft: 10,
    color: "#05375a",
  },
  errorMsg: {
    color: "#FF0000",
    fontSize: 14,
  },
  button: {
    alignItems: "center",
    marginTop: 50,
  },
  signIn: {
    width: "100%",
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
  },
  textSign: {
    fontSize: 18,
    fontWeight: "bold",
  },
});

export default LoginScreen;
