import React from "react";
import { StyleSheet, Text, View, ScrollView } from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import HeaderButton from "../components/HeaderButton";

const DashboardScreen = (props) => {
  return (
      <View style={styles.container}>
        <Text>deneme</Text>
      </View>
  );
};

DashboardScreen.navigationOptions = (navData) => {
  return {
    headerTitle: "Anasayfa",
    headerLeft: () => (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Menu"
          iconName="ios-menu"
          onPress={() => {
            navData.navigation.toggleDrawer();
          }}
        />
      </HeaderButtons>
    ),
  };
};

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  text_header: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 30,
    marginTop: 20,
  },
});

export default DashboardScreen;
